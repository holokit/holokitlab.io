# gitbook legacy editor

below are the command lines to be run for the installation of the gitbook


### linux installation

```bash
version=1.1.0
arch=linux64
xdg-open https://github.com/GitbookIO/editor-legacy/releases

mkdir -p gitbook-editor/releases
cd gitbook-editor
curl -o releases/gitbook-${arch}-v${version}.tar.gz https://github.com/GitbookIO/editor-legacy/releases/download/${version}/gitbook-${arch}.tar.gz
tar zxf releases/gitbook-${arch}-v${version}.tar.gz
cd $arch
bash install.sh

# to run the gitbook editor: 
$HOME/.gitbook/versions/current/start.sh

```


### MacOS installation


```sh
version=1.1.0
arch=mac
open https://github.com/GitbookIO/editor-legacy/releases

mkdir -p gitbook-editor/releases
cd gitbook-editor
curl -o releases/gitbook-${arch}-v${version}.dmg https://github.com/GitbookIO/editor-legacy/releases/download/${version}/gitbook-${mac}.dmg
sudo hdiutil attach releases/gitbook-${arch}-v{$version}.dmg

cd /Volumes/gitbook-${arch}-v${version}
sudo cp -rf gitbook-editor.app /Applications
sudo hdiutil detach /Volumes/gitbook-${arch}-v${version}
echo ... anything else ?

```


### Windows installation

```powershell
# TODO ...

```

