---
---
<meta charset="utf8"/>
# holoKit

This is the package manager for the holoSphere OS


for the installation of gitbook see also [*](https://www.one-tab.com/page/ghwlJZ16TwW4PQUdmF7wzw)





**note**: this repository resides on 

- github: at <git@github.com:holoTools/holoKit.git> and it is encrypted,
- gitlab: at 
- gitea : at  <git@localhost:michelc/holoKIT.git> or <http://gitea.localhost:3000/michelc/holoKIT.git>
- hologit: at http://gitea.localhost:8080/ipns/k2k4r8l6gbwwjvcz6a9mxrm6zndz8aaisnn92x2z4kkn8pdaekec72m4/holokit.git


clear-text source are available on gitea and hologit (<https://holoGIT.ml/>)

If you want to decypher this repository you can

1. checkout the objects folder ands
2.  mount it as .git

```sh
git clone git@github.com:holoTools/holoKit.git
ipfs get -o .secure QmPbNPYHjcUYwAGyJUq5ZbQhWbxWeMfBdHCvW5YCHT43KE
cd holoKit
cryfs -c .secure/config.key objects .git -- -o nonempty

```

... TBD

