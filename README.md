---
qm: QmNZXYTs7rZRVvd7E7iPcbaw1CSVkDERZmZyWd4VrHiUbD
---
# holoKIT 

* this page is better view on the git-pages at :
  <https://holoVerse.gitlab.io/holoTools/holoKit/README.html>

**HoloKit** is a set of softwares we use to evolve each one of us toward a conscious humanity.


We use tools and software that are openSource and protable across plateform
as we want them to be universally used within each holoVerse.

Therefore we deliver softwares primarly in JS, Go, (kotlin), bash and perl.

There are a few requirements :

You need to have "shell" access to your computer, i.e. being able
to run [bash] commandes.


### Quick INSTALL 

If you run an IPFS node you can access the tools
here : <https://gateway.ipfs.io/ipfs/{{page.qm}}>

otherwise check the [INSTALL](INSTALL.md) file.

### Slow INSTALL

...

### Assisted INSTALL

if you run into problem using our tools, we are happy to help you sort it out,
and get you a working installation on your plateform.

contact us at michelc@gc-bank.org to schedule a time.



