#

lsb_release --all



if false; then

sudo apt-get install libncurses5-dev libncursesw5-dev
sudo apt-get install gnome-devel
sudo apt-get build-dep vim-gnome
sudo apt-get install vim-runtime

 # from source ...
if [ ! -d vim/src ]; then
git clone https://github.com/vim/vim.git
fi
cd vim
make distclean
cd ../vim/src
git pull
./configure --enable-gui=yes --enable-perlinterp=dynamic
make -j 4
cd ../../vim
#./src/vim -g 
# /!\ TODO : find out a way to compile it with the proper dependencies !
#sudo make install
#make clean

fi

if false; then
 if [ ! -e downloads/vim-8.2.tar.bz2 ]; then
 (cd downloads && curl --remote-name https://ftp.nluug.nl/pub/vim/unix/vim-8.2.tar.bz2)
 fi
 bzcat downloads/vim-8.2.tar.bz2 | tar xfv - 
 cd vim/src
 ./configure --enable-gui=yes --enable-perlinterp=dynamic
 make -j 4
 # failed too !
fi

if false; then
 cd downloads
 curl --remote-name http://ftp.ch.debian.org/debian/pool/main/v/vim/vim-gtk3_8.2.0716-3_amd64.deb
 sudo apt-get install vim-gtk3_8.2.0716-3_amd64.deb
 # failed too !
 cd ..
fi

 # ALTERNATIVE:
 sudo apt install vim-gtk3

## initial setup
mkdir $HOME/.vim
mkdir $HOME/.vim/backups
mkdir $HOME/.vim/swapfiles
mkdir $HOME/.vim/undo


if false; then
 curl --remote-name http://ftp.ch.debian.org/debian/pool/main/v/vim-ledger/vim-ledger_1.2.0-1_all.deb
fi

true;
