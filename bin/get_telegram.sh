#

ver='2.4.1';
if [ -d downloads ]; then DWN=downloads; else DWN=.; fi

if [ ! -s $HOME/Downloads/tsetup.$ver.tar.xz ]; then
cp -p $HOME/Downloads/tsetup.$ver.tar.xz $DWN/
fi

if [ ! -e $DWN/tsetup.$ver.tar.xz ]; then
cd $DWN/
curl https://updates.tdesktop.com/tlinux/tsetup.${ver}.tar.xz
cd ..
fi

xz -d --stdout $DWN/tsetup.${ver}.tar.xz | tar xfv -
./Telegram/Telegram

