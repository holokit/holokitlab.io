#

tic=$(date +%s)
# /!\ use diodon instead
pgm=clipit
pgm=diodon
ver="1.0"

# see
# [1]: https://qwant.com/?q=get_${pgm}.sh+holoKit
install="sudo apt-get install $pgm -y"
#install="xdg-open apt://$pgm"
if ! which $pgm; then
#sudo apt-get install xref
sudo apt-get install xclip
sudo apt-get install zeitgeist
sudo apt-get install diodon
#$install
fi


qm=$(ipfs add -Q -w $(which $pgm) $0)
echo $tic: $qm;
exit $?;
