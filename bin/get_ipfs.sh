#

echo $0: ...

if [ "x$COREDIR" != 'x' ]; then
cd $COREDIR
fi

if [ ! -e downloads/go-ipfs_v0.7.0_linux-amd64.tar.gz ]; then
  curl -L -o downloads/go-ipfs_v0.7.0_linux-amd64.tar.gz https://dist.ipfs.io/go-ipfs/v0.7.0/go-ipfs_v0.7.0_linux-amd64.tar.gz
fi
tar xfv downloads/go-ipfs_v0.7.0_linux-amd64.tar.gz
go-ipfs/ipfs init
screen -mS $CORE  go-ipfs/ipfs daemon &
true;

ipfs get -o $HOME/.ipfsignore QmUAf7KZ7NMYwajPrBG6g2VLxBmAjrnXXETMwjVbUsBmP5
ipfs config --json API.HTTPHeaders.Access-Control-Allow-Origin '["http://localhost:8080","http://localhost"]'

