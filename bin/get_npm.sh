#

pgm=npm
ver="1.0"
tic=$(date +%s)

# ---------------------------------------
nix=$(which xdg-open)
if [ "x$nix" != 'x' ]; then
#install="sudo apt-get install npm -y"
install="xdg-open apt://npm"

$install

else
# ---------------------------------------
mac=$(which open)
if [ "x$mac" != 'x' ]; then
install="open https://git-scm.org"
$install

fi
# ---------------------------------------
fi


# see also
# [1]: https://qwant.com/?q=get_${pgm}.sh+holoKit

if which ipfs 1>/dev/null; then
qm=$(ipfs add -Q -w $(which $pgm) $0)
echo $tic: $qm;
fi

exit $?;
