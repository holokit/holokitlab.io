#

pgm=brave-browser
ver="1.0"
tic=$(date +%s)
# see
# [1]: https://qwant.com/?q=get_${pgm}.sh+holoKit
# [2]: https://brave.com/

#install="sudo apt-get install $pgm -y"
install="xdg-open apt://$pgm"

## 1 prerequisites :
sudo apt install apt-transport-https curl

if ! which $pgm; then

## added keyring ...
if [ ! -e /etc/apt/sources.list.d/${pgm}-release.list ]; then
curl -s https://brave-browser-apt-release.s3.brave.com/brave-core.asc | sudo apt-key --keyring /etc/apt/trusted.gpg.d/brave-browser-release.gpg add -
echo "deb [arch=amd64] https://brave-browser-apt-release.s3.brave.com/ stable main" | sudo tee /etc/apt/sources.list.d/brave-browser-release.list
fi

sudo apt update
sudo apt install $pg-browserm
fi


qm=$(ipfs add -Q -w $(which $pgm) $0)
echo $tic: $qm;


exit $?;
