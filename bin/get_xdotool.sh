#

tic=$(date +%s)
pgm=xdotool
ver="1.0"

# see
# [1]: https://qwant.com/?q=get_${pgm}.sh+holoKit
install="sudo apt-get install $pgm -y"
install="xdg-open apt://$pgm"
if ! which $pgm; then
$install
fi


qm=$(ipfs add -Q -w $(which $pgm) $0)
echo $tic: $qm;
exit $?;
