#

pgm=docket
ver="1.0"
tic=$(date +%s)

open=$(which xdg-open)
# ---------------------------------------
if which dnf 1>/dev/null; then
echo "info: not tested yet ..."
install="sudo dnf install docker.io -y"
$install
else
# ---------------------------------------
# Debian / Ubuntu
# read [1](https://docs.docker.com/engine/install/ubuntu/)
if [ "x$open" != 'x' ]; then
lsb_release -a
# pre-requisites 
sudo apt-get remove docker docker-engine docker.io containerd runc
#sudo rm -rf /var/lib/docker
apt-get update
sudo apt-get install \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg-agent \
    software-properties-common
# ---------------------
if [ "$(lsb_release -cs)" = 'groovy' ]; then
# see also [1](https://superuser.com/questions/1588174/docker-with-ubuntu-20-10-strconv-atoi-parsing-0-beta1-invalid-syntax)
sudo apt install docker.io

debf=docker-ce_18.06.3~ce~3-0~ubuntu_amd64.deb
deb_url=https://download.docker.com/linux/ubuntu/dists/artful/pool/stable/amd64/$debf
if [ ! -d downloads ]; then mkdir downloads; fi
curl -L -o download/$debf $deb_url
sudo dpkg -i download/$debf

sudo systemctl unmask docker
sudo service docker start
sudo docker run hello-world
## successful !

else
# ---------------------
# package GPG key :
qm=$(curl -fsSL https://download.docker.com/linux/ubuntu/gpg | ipfs add -Q -n -)
echo keyhash: $qm
ipfs cat /ipfs/$qm | gpg --with-colons
echo "providers: $( ipfs dht findprovs /ipfs/$qm -n 1 )"
ipfs cat /ipfs/QmdfNVRQUYhqkV5JVbYPbFLnKBUF5LeHyh5LpXm7inxkXg/key_docker.pub | sudo apt-key add -
sudo apt-key fingerprint 0EBFCD88
# add repository
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"

sudo apt-get install docker-ce docker-ce-cli containerd.io
fi



url="apt://$pgm"
echo "go to $url; follow the instructions;"
echo " and if successfull type 'enter'"
install="xdg-open $url"
$install
read ans


#install="sudo apt-get install docker.io docker-ce docker-ce-cli containerd.io -y"

else
# ---------------------------------------
open=$(which open)
if [ "x$open" != 'x' ]; then
sw_vers
url=https://docker.com
echo "go to $url; follow the instructions;"
echo " and if successfull type 'enter'"
install="open $url"

which xcodebuild
xcodebuild -version
xcode-select -version
xcode-select -print-path

which docker
docker --version


$install
read ans

fi
# ---------------------------------------
fi
fi

# see also
# [1]: https://qwant.com/?q=get_${pgm}.sh+holoKit

if which ipfs 1>/dev/null; then
qm=$(ipfs add -Q -w $(which $pgm) $(which $0))
echo $tic: $qm;
fi

exit $?;
