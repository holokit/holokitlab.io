#

pgm=vlc
ver="1.0"
#install="sudo apt-get install vlc vlc-bin -y"
install="xdg-open apt://vlc"

$install

exit $?;

tic=$(date +%s)
if [ -d downloads ]; then DWN=downloads; else DWN=.; fi


# see
# [1]: https://qwant.com/?q=get_software.sh+holoKit
# [2]: https://linuxhint.com/install_vlc_debian/
url=https://github.com/holoKit/$pgm/archive/${pgm}-v${ver}.tar.gz
if ! which $pgm; then
if [ ! -e $DWN/${pgm}-${ver}.tar.gz ]; then
cd $DWN
curl -L --remote-name $url
cd ..
fi
tar zxf $DWN/${pgm}-${ver}.tar.gz
cd ${pgm}-${ver}
CPROG=$pgm
PREFIX=${COREDIR:-$(pwd -P)/..}
EXEC_PREFIX=${PREFIX}
BINDIR=${EXEC_PREFIX}/bin
DATAROOTDIR=${PREFIX}/share
DOCDIR=${DATAROOTDIR}/doc/${CPROG}
SYSCONFDIR=${PREFIX}/etc
HTMLDIR=${DOCDIR}
INCLUDEDIR=${DESTDIR}${PREFIX}/include
LIBDIR=${DESTDIR}${EXEC_PREFIX}/lib
PID_FILE=/var/run/${CPROG}.pid
make -E CORE=${CORE}

#sudo make install
#install: ${HTMLDIR}/index.html ${SYSCONFDIR}/civetweb.conf
#install -d -m 755  "${DOCDIR}"
#install -m 644 *.md "${DOCDIR}"
#install -d -m 755 "${BINDIR}"
#install -m 755 ${CPROG} "${BINDIR}/"
cd ..
screen -mS $pgm sudo bin/$pgm etc/$pgm.conf -run_as_user $USER

qm=$(ipfs add -Q -w $(which $pgm) $0)
echo $tic: $qm;
fi
