#

pgm=gitea
ver="1.0"
tic=$(date +%s)

open=$(which xdg-open)
# ---------------------------------------
# Fedora *nix
if which dnf 1>/dev/null; then
install="sudo dnf install $pgm -y"
$install
else
# ---------------------------------------
# Ubuntu *nix
if [ "x$open" != 'x' ]; then
lsb_release -a

HOMEDIRS=${HOME%/*}
sudo adduser --system --shell /bin/bash --gecos 'Git Version Control' --group --disabled-password --home $HOMEDIRS/git git
cd $HOMEDIRS/git 
sudo -u git wget https://dl.gitea.io/gitea/1.12.1/gitea-1.12.1-linux-amd64.asc
sudo -u git wget -O gitea https://dl.gitea.io/gitea/1.12.1/gitea-1.12.1-linux-amd64
sudo -u git chmod +x gitea
key=$(gpg --with-colons --no-tty --search-keys teabot@gitea.io 2>/dev/null | grep 'pub:' | cut -d':' -f2)
gpg --keyserver keys.openpgp.org --recv $key
if ! gpg --verify gitea-1.12.1-linux-amd64.asc gitea; then
  echo "bad signature !" 
  exit $(expr $$ % 256)
fi
# db install
sudo apt-get install sqlite3 libsqlite3-dev
sudo apt-get install sqlitebrowser
cd /var/lib/gitea/data
sqlite3 gitea.db <<EOT
.databases
.quit
EOT
export GITEA_WORK_DIR=/var/lib/gitea
mkdir -p $GITEA_WORK_DIR/{custom,data,log}
chown -R git:git $GITEA_WORK_DIR/
chmod -R 750 $GITEA_WORK_DIR
mkdir /etc/gitea
chown root:git /etc/gitea
chmod 770 /etc/gitea

cp -p gitea /usr/local/bin/gitea









url="apt://$pgm"
echo "go to $url; follow the instructions;"
echo " and if successfull type 'enter'"
install="xdg-open $url"
$install
read ans




#install="sudo apt-get install git-all -y"

else
# ---------------------------------------
# macOS
open=$(which open)
if [ "x$open" != 'x' ]; then
sw_vers
url=https://git-scm.org
echo "go to $url; follow the instructions;"
echo " and if successfull type 'enter'"
install="open $url"

which xcodebuild
xcodebuild -version
xcode-select -version
xcode-select -print-path

which gitbook
gitbook ls
git --version


$install
read ans

fi
# ---------------------------------------
fi
fi

# more info
echo see also https://qwant.com/?q=get_${pgm}.sh+holoKit


if which ipfs 1>/dev/null; then
qm=$(ipfs add -Q -w $(which $pgm) $(which $0))
echo $tic: $qm;
fi

exit $?;
