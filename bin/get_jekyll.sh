#

tic=$(date +%s)
lsb_release --all
os=$(echo `lsb_release -ris`)
echo "--- # $0 ($os)"
if [ -d downloads ]; then DWN=downloads; else DWN=.; fi


# see
# [1]:
pgm=jekyll
ver="4.0"
url=https://duckduckgo.com/?q=$pgm+installation+${ver}
if ! which $pgm; then

sudo apt-get install jekyll
## update jekyll ecosystem
sudo gem update --system --no-document 
sudo gem install bundler
# see : [2](https://talk.jekyllrb.com/t/new-install-jekyll-4-0-0-on-ubuntu-19-04-gives-error/3262/2)
if [ -e /usr/lib/ruby/vendor_ruby ]; then
sudo mv /usr/lib/vendor_ruby /usr/lib/vendor_ruby_removed
gem install sass -v '3.7.4'
fi
if [ -e Gemfile ]; then
bundler update --bundler
rm Gemfile.lock
bundler install
fi
sudo gem install kramdown-parser-gfm

qm=$(ipfs add -Q -w $(which $pgm) $0)
echo $tic: $qm;
fi
