#
tic=$(date +%s)
if [ -d downloads ]; then DWN=downloads; else DWN=.; fi


# see
# [1]: https://github.com/civetweb/civetweb/blob/master/docs/Installing.md
# [2]: https://github.com/civetweb/civetweb/releases
pgm=civetweb
ver="1.13"
url=https://github.com/civetweb/civetweb/archive/v${ver}.tar.gz
if ! which $pgm; then

if [ ! -e $DWN/civetweb-${ver}.tar.gz ]; then
cd $DWN
curl -L --remote-name $url
mv v${ver}.tr.gz civetweb-${ver}.tar.gz
cd ..
fi
tar zxf $DWN/civetweb-${ver}.tar.gz
cd civetweb-${ver}
CPROG=$pgm
PREFIX=${COREDIR:-$(pwd -P)/..}
EXEC_PREFIX=${PREFIX}
BINDIR=${EXEC_PREFIX}/bin
DATAROOTDIR=${PREFIX}/share
DOCDIR=${DATAROOTDIR}/doc/${CPROG}
SYSCONFDIR=${PREFIX}/etc
HTMLDIR=${DOCDIR}
INCLUDEDIR=${DESTDIR}${PREFIX}/include
LIBDIR=${DESTDIR}${EXEC_PREFIX}/lib
PID_FILE=/var/run/${CPROG}.pid
make -E NO_SSL=1 -E PORTS=80 

#sudo make install
#install: ${HTMLDIR}/index.html ${SYSCONFDIR}/civetweb.conf
#install -d -m 755  "${DOCDIR}"
#install -m 644 *.md "${DOCDIR}"
#install -d -m 755 "${BINDIR}"
install -m 755 ${CPROG} "${BINDIR}/"
cd ..
screen -mS CIVET sudo bin/civetweb etc/civetweb.conf -listening_ports 80 -run_as_user $USER

qm=$(ipfs add -Q -w $(which $pgm) $0)
echo $tic: $qm;
fi
