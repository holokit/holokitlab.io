

echo -n "samba: "

# see also [1]: https://tldp.org/HOWTO/SMB-HOWTO-7.html

if ! which samba 2>null; then
sudo apt-get install samba smbclient
# sambu is a samba user used for remote mounting partition on machine w/o same users
sudo adduser --shell /bin/false --no-create-home \
 --disabled-password --disabled-login sambu
sudo chfn -f "Samba Account" sambu
sudo smbpasswd -a sambu

sudo service smbd stop
sudo service smbd start
sudo service smbd restart
service --status-all  | grep smb


smbclient -U sambu -L ocean
smbclient -U $USER '\\ocean\USB'

# on Windows side
# net use * /DELETE /y
# net use \\ocean\USB /USER:michelc
fi

