#

# see also: [1](https://keybase.io/docs/the_app/install_linux)
echo $0: ...

if [ "x$COREDIR" != 'x' ]; then
cd $COREDIR
fi

# install latest NodeSource Node.js:
#curl -sL https://deb.nodesource.com/setup | sudo bash -
qm=$(ipfs add -Q https://deb.nodesource.com/setup)
#curl -sL https://deb.nodesource.com/setup_14.x | sudo bash -
qm=$(ipfs add -Q https://deb.nodesource.com/setup_14.x)
echo qm: $qm
if [ "z$qm" = 'zQmaREbs15uB8EcFeQzkUgxNmCxUVBr8SjF7xsiAufWWRUR' ]; then
ipfs cat $qm | sudo bash -
fi

DEB=keybase_amd64.deb
url=https://prerelease.keybase.io/$DEB
if [ ! -e downloads/$DEB ]; then
  curl -L -o downloads/$DEB $url
fi
sudo apt install downloads/$DEB
# failed !

sudo apt-get install libappindicator1
sudo apt --fix-broken install
sudo dpkg -i downloads/$DEB
# this one worked !
true;

