#

# intent: set [CORS] for ipfs
#
# problem was:
# Cross-Origin Request Blocked: The Same Origin Policy disallows
# reading the remote resource at http://localhost:8080/ipfs/.
# (Reason: Multiple CORS header ‘Access-Control-Allow-Origin’ not allowed).
#

# see also
# [1](https://duckduckgo.com/?q=IPFS+cors+setup&t=newext&atb=v241-5ck&ia=web)

# "http://localhost:8080",
# "http://localhost:3000",
# "http://localhost",
# "https://webui.ipfs.io",
# "null"

# being "explicit" doesn't work w/ IPFS /!\
#ipfs config --json API.HTTPHeaders.Access-Control-Allow-Origin '["http://localhost:8080","http://localhost"]'
ipfs config --json API.HTTPHeaders.Access-Control-Allow-Origin '["*"]'
ipfs config --json API.HTTPHeaders.Access-Control-Allow-Methods '["GET", "POST"]'
ipfs config --json API.HTTPHeaders.Access-Control-Allow-Headers '["Authorization"]'
ipfs config --json API.HTTPHeaders.Access-Control-Expose-Headers '["Location"]'
ipfs config --json API.HTTPHeaders.Access-Control-Allow-Credentials '["true"]'


#ipfs shutdown
ipfsd.sh stop
ipfsd.sh start
