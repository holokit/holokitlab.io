## Bookstack Installation

1. install php

```sh
cd holoKit

sudo apt install php7.2-cli
sudo apt-get install php-curl
sudo apt-get install php-dompdf
sudo apt-get install php-mbstring
sudo apt-get install php-tidy
sudo apt-get install php-xml


top=$(git rev-parse --show-toplevel)
bin=$top/bin
if [ ! -e $bin ]; then mkdir $bin; fi
export PATH=$bin:$PATH

```

2. install MySQL
sudo apt-get install php7.2-mysql mysql-server-5.7 
DB_PASS="$(head /dev/urandom | tr -dc A-Za-z0-9 | head -c 13)"
sed -i.bak 's/DB_DATABASE=.*$/DB_DATABASE=bookstack/' .env
sed -i.bak 's/DB_USERNAME=.*$/DB_USERNAME=bookstack/' .env
sed -i.bak "s/DB_PASSWORD=.*\$/DB_PASSWORD=$DB_PASS/" .env

sudo mysql -u root --execute="CREATE DATABASE bookstack;"
sudo mysql -u root --execute="CREATE USER 'bookstack'@'localhost' IDENTIFIED BY '$DB_PASS';"
sudo mysql -u root --execute="GRANT ALL ON bookstack.* TO 'bookstack'@'localhost';FLUSH PRIVILEGES;"

3. install composer (see also [*](https://getcomposer.org/download/)

```sh
mkdir composer
cd composer
php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
php -r "if (hash_file('sha384', 'composer-setup.php') === '8a6138e2a05a8c28539c9f0fb361159823655d7ad2deecb371b04a83966c61223adc522b0189079e3e9e277cd72b8897') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
php composer-setup.php --install-dir=../bin  --filename=composer
#php -r "unlink('composer-setup.php');"

```

4. install bookstack

```
if [ ! -e $top/bookstack ]; then mkdir $top/bookstack; fi
cd $top/bookstack
git clone https://github.com/BookStackApp/BookStack.git --branch release --single-branch .
composer install --no-dev
chgrp -R www-data storage bootstrap/cache public/uploads
chmod g+rw storage bootstrap/cache public/uploads
php artisan key:generate
php artisan migrate --no-interaction --force

```

### error messages :

```
    - The requested PHP extension ext-dom * is missing from your system. Install or enable PHP's dom extension.
    - The requested PHP extension ext-gd * is missing from your system. Install or enable PHP's gd extension.
    - The requested PHP extension ext-mbstring * is missing from your system. Install or enable PHP's mbstring extension.
    - The requested PHP extension ext-tidy * is missing from your system. Install or enable PHP's tidy extension.
    - The requested PHP extension ext-xml * is missing from your system. Install or enable PHP's xml extension.
    - Installation request for aws/aws-sdk-php 3.134.3 -> satisfiable by aws/aws-sdk-php[3.134.3].
    - aws/aws-sdk-php 3.134.3 requires ext-simplexml * -> the requested PHP extension simplexml is missing from your system.
    - Installation request for dompdf/dompdf v0.8.5 -> satisfiable by dompdf/dompdf[v0.8.5].
    - dompdf/dompdf v0.8.5 requires ext-dom * -> the requested PHP extension dom is missing from your system.
    - Installation request for facade/ignition 1.16.1 -> satisfiable by facade/ignition[1.16.1].
    - facade/ignition 1.16.1 requires ext-mbstring * -> the requested PHP extension mbstring is missing from your system.
    - Installation request for gathercontent/htmldiff 0.2.1 -> satisfiable by gathercontent/htmldiff[0.2.1].
    - gathercontent/htmldiff 0.2.1 requires ext-tidy * -> the requested PHP extension tidy is missing from your system.
    - Installation request for laravel/framework v6.18.3 -> satisfiable by laravel/framework[v6.18.3].
    - laravel/framework v6.18.3 requires ext-mbstring * -> the requested PHP extension mbstring is missing from your system.
    - Installation request for league/commonmark 1.4.2 -> satisfiable by league/commonmark[1.4.2].
    - league/commonmark 1.4.2 requires ext-mbstring * -> the requested PHP extension mbstring is missing from your system.
    - Installation request for scrivo/highlight.php v9.18.1.1 -> satisfiable by scrivo/highlight.php[v9.18.1.1].
    - scrivo/highlight.php v9.18.1.1 requires ext-mbstring * -> the requested PHP extension mbstring is missing from your system.
    - Installation request for tijsverkoyen/css-to-inline-styles 2.2.2 -> satisfiable by tijsverkoyen/css-to-inline-styles[2.2.2].
    - tijsverkoyen/css-to-inline-styles 2.2.2 requires ext-dom * -> the requested PHP extension dom is missing from your system.
    - aws/aws-sdk-php 3.134.3 requires ext-simplexml * -> the requested PHP extension simplexml is missing from your system.
    - league/flysystem-aws-s3-v3 1.0.24 requires aws/aws-sdk-php ^3.0.0 -> satisfiable by aws/aws-sdk-php[3.134.3].
    - Installation request for league/flysystem-aws-s3-v3 1.0.24 -> satisfiable by league/flysystem-aws-s3-v3[1.0.24].


```
