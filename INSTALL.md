## HoloKIT installation 

*holo*kit is the holosphere OS package manager.


The kit is comprise of 6 essentials tools:

- IPFS : see installation guide: [IPFS/INSTALL.md](IPFS/INSTALL.md)
- GIT : see installation guide: [GIT/INSTALL.md](GIT/INSTALL.md)

- gitbook-legacy : see installation guide: [git-book]
- BookStack : see installation guide : [bookstack/INSTALL.md](bookstack/INSTALL.md)

- holoWiki
-


